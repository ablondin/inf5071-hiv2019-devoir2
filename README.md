# Devoir 2

Ce dépôt contient les fichiers nécessaires à la réalisation du devoir 2.

## Marche à suivre

L'énoncé complet du devoir 2 est disponible [ici](./devoir2.pdf).

Pour le compléter, vous n'avez qu'à cloner le dépôt présent et à travailler
directement sur la version clonée pour la remise. Cela simplifiera la
récupération des fichiers nécessaires pour le devoir et il sera aussi plus
facile de faire une mise à jour (grâce à la commande `git pull`) dans le cas où
j'apporterais des rectifications/corrections à l'énoncé.

## Contenu

* [`devoir2.pdf`](./devoir2.pdf), qui contient l'énoncé du devoir
* [`devoir2.tex`](./devoir2.tex), qui contient le gabarit LaTeX du devoir que
  vous pouvez utilisez pour insérer vos solutions, si vous le souhaitez
* Des exemples de cartes de normales dans le répertoire [`images/`](./images)
* [`rayon.py`](./rayon.py), qui contient le programme Python de départ pour la
  question 2
* Le vidéo `spirale.webm`, qui illustre ce qui est demandé à la question 3(a):

  ![](movies/spirale.webm)

* Le vidéos `3-spirales.webm` qui illustre ce qui est demandé à la question
  3(b):

  ![](movies/3-spirales.webm)

